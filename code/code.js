// Додати нові продукти (Відео для відео хостингу та страви для ресторану)
// Відео має бути 3 зовнішнім посиланням та 3 відео завантажені до проекту

// Тут реалізація вашого коду.
import { generationId, dateNow } from "./functions.js";

export const saveVideoAndRestData = e => {
    try {
        const [isCategory] = document.querySelector("select").selectedOptions;
        const [...inputs] = document.querySelectorAll('form input');
        // console.log(isCategory.value)
        // console.log(inputs)
        if (isCategory.value === 'Відео хостинг') {
            console.log('video')
            const obj = {
                productName: 'string',
                poster: 'string',
                url: 'string',
                description: 'string',
                keywords: 'string array',
            }

            inputs.forEach(el => {
                obj[el.dataset.type] = el.value;
                el.value = '';
            })

            const video = JSON.parse(localStorage.video);
            if (video.length === 0) {
                video.push(new videoElementCrm('Jellyfish', '', '/video/jellyfish.mp4', 'swimming jellyfish', 'медузи, jellyfish', dateNow, generationId));
                video.push(new videoElementCrm('Sheeps', '', '/video/sheep.mp4', 'sheeps in the field', 'вівці, мала вівця', dateNow, generationId));
                video.push(new videoElementCrm('Beach', '', '/video/beach.mp4', 'view on the beachside', 'пляж, beach, beachside', dateNow, generationId));
                video.push(new videoElementCrm('РІК – авторський документальний проєкт Дмитра Комарова', 'https://images.1plus1.video/playlist-1/168516/c661d57b51955e98cc000d8e3dc013c3.768x576.jpg', 'https://www.youtube.com/watch?v=ER6Hs4rSyAg', 'це авторський погляд Дмитра Комарова на війну з тих ракурсів, які не побачиш в новинах.', 'війна, документалка, Комаров', dateNow, generationId));
                video.push(new videoElementCrm('Маріуполь. Невтрачена надія', 'https://cdn4.suspilne.media/images/resize/600x1.78/d4fec207c3df0aea.jpg', 'https://www.youtube.com/watch?v=q05wJtU6uEs', 'Кіно є свідченнями російсько-української війни, очима простих людей, які прожили у Маріуполі перший місяць вторгнення', 'документалка, війна з руснею', dateNow, generationId));
                video.push(new videoElementCrm('Тракторець молодець та бик забіяка', 'https://www.febspot.com/contents/videos_screenshots/311000/311960/preview_720p.mp4.jpg', 'https://www.youtube.com/watch?v=BxbNdErl_Tw', 'Як тракторець молодець, разом з друзями, приборкали бика - хулігана', 'трактор, бик', dateNow, generationId));
            }

            video.push(new videoElementCrm(obj.productName, obj.poster, obj.url, obj.description, obj.keywords, dateNow, generationId));
            localStorage.video = JSON.stringify(video);
            
        } else if (isCategory.value === 'Рестаран') {
            
            const obj = {
                productName: 'string',
                productWeigth: 'number',
                ingridients: 'string',
                description: 'string',
                price: 'number',
                productimageUrl: 'string',
                keywords: 'string array',
            }

            inputs.forEach(el => {
                obj[el.dataset.type] = el.value;
                el.value = '';
            })

            const rest = JSON.parse(localStorage.restorationBD);
            rest.push(new restoranElementCrm(obj.productName, obj.productWeigth, obj.ingridients, obj.description, obj.price, obj.productimageUrl, obj.productQuantity, obj.keywords, dateNow, generationId));
            localStorage.restorationBD = JSON.stringify(rest);
        }
    } catch (error) {
        console.error(error)
    }

}

class videoElementCrm {
    constructor(videoName = '', poster = '', url = '', description = '', keywords = [], dateNow = () => { }, id = () => { }) {
        this.id = id();
        this.date = dateNow();
        this.videoName = videoName;
        this.poster = poster;
        this.url = url;
        this.description = description;
        this.keywords = keywords.split(',');
        this.status = false;
    }
}

class restoranElementCrm {
    constructor(productName = '', productWeigth = 0, ingridients = '', description = '', price = 0, productimageUrl = '', productQuantity = 0, keywords = [], dateNow = () => { }, id = () => { }) {
        this.id = id();
        this.date = dateNow();
        this.productName = productName;
        this.productWeigth = productWeigth;
        this.ingridients = ingridients;
        this.description = description;
        this.price = price;
        this.productimageUrl = productimageUrl;
        this.productQuantity = productQuantity;
        this.keywords = keywords.split(',');
        this.status = false;
    }
}
