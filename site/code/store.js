const store = JSON.parse(localStorage.store);
console.log(store);

if(!Array.isArray(store)){
    throw Error ("...")
}

const storeEl = store.map(({ productName, id, productDescription, keywords, productImage, porductPrice }) => {
    return `
    <div class="store">
    <h3 class="store-name">${productName}</h3>
    <div class='store-picture'><img class='store-img' id="${id}" src="${productImage}" alt="${productName}"></div>
    <p class="store-description">${productDescription}</p>
    <p class="store-price">Ціна: ${porductPrice} грн.</p>
    <div class='store-keyw'>
    ${keywords.map((el) => {
        return `<span class="badge bg-secondary">${el}</span>`
    }).join("")}
     </div>  
    </div>
    `
})

document.querySelector(".store-box")
.insertAdjacentHTML("beforeend", storeEl.join(""));