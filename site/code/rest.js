const rest = JSON.parse(localStorage.restorationBD);
console.log(rest);

if(!Array.isArray(rest)){
    throw Error ("...")
}

const restEl = rest.map(({ productName, id, ingridients, description, keywords, productimageUrl, productWeigth, price }) => {
    return `
    <div class="rest">
    <h3 class="rest-name">${productName}</h3>
    <div class='rest-picture'><img class='img' id="${id}" src="${productimageUrl}" alt="${productName}"></div>
    <p class="rest-description">${description}</p>
    <p class="rest-ingridients">Складові: ${ingridients}</p>
    <p class="rest-productweigth">Вага: ${productWeigth}г</p>
    <p class="rest-price">Ціна: ${price} грн.</p>
    <div class='rest-keyw'>
    ${keywords.map((el) => {
        return `<span class="badge bg-secondary">${el}</span>`
    }).join("")}
     </div>  
    </div>
    `
})

document.querySelector(".rest-box")
.insertAdjacentHTML("beforeend", restEl.join(""));

